/**
 *
 * Copyright 2015 IBM Corp. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;
const PAGE_ID = process.env.PAGE_ID;
const APP_ID = process.env.APP_ID;
const APP_SECRET = process.env.APP_SECRET;
const VERIFY_TOKEN = process.env.VERIFY_TOKEN;
const APP_URL = process.env.APP_URL;
const SHOP_URL = process.env.SHOP_URL;


var express = require('express'); // app server
var bodyParser = require('body-parser'); // parser for post requests
var cors = require('cors');

const crypto = require("crypto");
const path = require("path");
const Receive = require("./services/receive");
const GraphAPi = require("./services/graph-api");
const User = require("./services/user");
const config = require("./services/config");
const i18n = require("./i18n.config");

const axios = require('axios');
const request = require('request');

var http = require('http');

var app = express();

app.use(bodyParser.json());
app.use(cors());


// Adds support for GET requests to our webhook
app.get("/webhook", (req, res) => {
  // Parse the query params
  let mode = req.query["hub.mode"];
  let token = req.query["hub.verify_token"];
  let challenge = req.query["hub.challenge"];

  // Checks if a token and mode is in the query string of the request
  if (mode && token) {
    // Checks the mode and token sent is correct
    if (mode === "subscribe" && token === config.verifyToken) {
      // Responds with the challenge token from the request
      console.log("WEBHOOK_VERIFIED");
      res.status(200).send(challenge);
    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
});


/* Handling all messenges */
app.post('/webhook', (req, res) => {
  if (req.body.object === 'page') {
    req.body.entry.forEach((entry) => {
      entry.messaging.forEach((event) => {
        if (event.message && event.message.text) {
          receivedMessage(event);
        }
      });
    });
    res.status(200).end();
  }
});

async function receivedMessage(event) {
  let sender = event.sender.id;
  let text = event.message.text;

  console.log('Miko Tes message: ', text);

  let isession = await getSession();
  let aiText = await sendMessageAi(text);

  console.log('Miko aiText message: ', aiText);

  prepareSendAiMessage(sender, aiText);

}

function sendMessage(messageData) {
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token: PAGE_ACCESS_TOKEN},
    method: 'POST',
    json: messageData
  }, (error, response) => {
    if (error) {
        console.log('Error sending message: ', error);
    } else if (response.body.error) {
        console.log('Error: ', response.body.error);
    }
  });
}


function prepareSendAiMessage(sender, aiText) {
  let messageData = {
    recipient: {id: sender},
    message: {text: aiText}
  };
  sendMessage(messageData);
}

async function sendMessageAi(textMsg) {

try {
  
  let inputPayload = {
          text: textMsg
  };

  let responseData = { ...inputPayload}; 

  let response = await axios.post(`https://covid-information-orchestrator.herokuapp.com/api/df_faq_query`,responseData,);

    console.log("Miko response.result", response.data.fulfillmentText);
    const data = response.data.fulfillmentText;
  
    console.log(data);
  
    return data;
}
catch (err) { console.log('Failed to send data to Watson API', err) }

}

async function sendMessageWatson(isession, textMsg) {

  console.log("Miko isession in sendMessageWatson", isession);

try {
  
  let inputPayload = {
      input: {
          message_type: 'text',
          text: textMsg,
      },
  };

  let responseData = { ...inputPayload, ...isession }; 

  let response = await axios.post(`https://coronavirus-info-orchestrator.herokuapp.com/api/message`,responseData,);

    const data = response.data.output.generic[0].text;
  
    console.log(data);
  
    return data;
}
catch (err) { console.log('Failed to send data to Watson API', err) }

}

async function getSession() {

  const response = await axios.get('https://coronavirus-info-orchestrator.herokuapp.com/api/session');

  const data = response.data;
  console.log(data);

  return data; 
}


module.exports = app;


